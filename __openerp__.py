# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution	
#    Copyright (C) 2013 - eric@vernichon.fr
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


{
    'name': 'Jasper Server',
    'version': '32',
    'category': 'Impression',
    'description': """This module for using JasperServer For Reports.

    """,
    'author': 'Everlibre',
    'website': 'http://openerp.com',
    'depends': ['base', 'web'],
    'init_xml': [],
    'data': [],
    'update_xml': [
        'security/ir.model.access.csv',
        'jasper_server_view.xml',
    ],
    'installable': True,
    'web_preload': True,
    'js': ['static/src/js/jasper_server.js'],
    
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
