# -*- coding: utf-8 -*-
##############################################################################
#
# Copyright (c) 2007 EVI All Rights Reserved.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsability of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# garantees and support are strongly adviced to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################
from report.render import render
from report.interface import report_int
from httplib2 import Http
import pooler
import cStringIO
import base64
from tools.translate import _
from dime import Message
from lxml import etree


class external_pdf(render):
    def __init__(self, pdf):
        render.__init__(self)
        self.pdf = pdf
        self.output_type = 'pdf'

    def _render(self):
        return self.pdf


class external_xls(render):
    def __init__(self, xls):
        render.__init__(self)
        self.xls = xls
        self.output_type = 'xls'

    def _render(self):
        return self.xls


class report_custom(report_int):
    def make_xml_safe(self, value):
        if isinstance(value, basestring):
            value = value.replace("&", "&amp;")
            value = value.replace("<", "&lt;")
            value = value.replace(">", "&gt;")
        return value
    
    def create(self, cr, uid, ids, data, context=None):
        if context is None:
            context = {}

        body_template = """<SOAP-ENV:Envelope
 xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"
 xmlns:xsd="http://www.w3.org/2001/XMLSchema"
 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
 xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/"
 xmlns:ns4="http://www.jaspersoft.com/client"
 SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
<SOAP-ENV:Body>
<ns4:runReport>
<request xsi:type="xsd:string">
    &lt;request operationName=&quot;runReport&quot;&gt;
        &lt;argument name=&quot;RUN_OUTPUT_FORMAT&quot;&gt;&lt;![CDATA[%s]]&gt;&lt;/argument&gt;
        &lt;argument name=&quot;USE_DIME_ATTACHMENTS&quot;&gt;
            &lt;![CDATA[1]]&gt;
        &lt;/argument&gt;
        &lt;resourceDescriptor name=&quot;&quot; wsType=&quot;reportUnit&quot; uriString=&quot;%s&quot; isNew=&quot;false&quot;&gt;
            &lt;label&gt;&lt;/label&gt;
            &lt;parameter name=&quot;REPORT_LOCALE&quot;&gt;&lt;![CDATA[%s]]&gt;&lt;/parameter&gt;
            %s
        &lt;/resourceDescriptor&gt;
        
    &lt;/request&gt;
</request></ns4:runReport>
</SOAP-ENV:Body>
</SOAP-ENV:Envelope>"""
        pool = pooler.get_pool(cr.dbname)
        conf_obj = pool.get('jasper.server.conf')
        conf_id = conf_obj.search(cr, uid, [], limit=1)
        conf = conf_obj.browse(cr, uid, conf_id)
        if not conf:
            raise Exception, _('You need to configure one server!')
        else:
            conf = conf[0]
        headers = {'Content-type': 'text/xml', 'charset': 'UTF-8', "SOAPAction": "runReport"}
        h = Http()
        
        lang = pool.get('res.lang').read(cr, uid, pool.get('res.lang').search(cr, uid, [('code', '=', context['lang'])]))
        param_lang = lang[0]['iso_code']
        parametres = [data['form']['params'][0], data['form']['params'][1], param_lang]  # format, URI, locale
        parametres.append("")
        for x in range(2, len(data['form']['params'])):
            nameparam = data['form']['params'][x]
            parametre = '&lt;parameter name="%s"&gt;<![CDATA[%s]]>&lt;/parameter&gt;' % (nameparam, self.make_xml_safe(data['form'][nameparam]))
            parametres[3] = parametres[3]+parametre
             
        body = body_template % tuple(parametres)
        h.add_credentials(conf.user, conf.pwd)
        try:
            resp, content = h.request(conf.name, "POST", body.encode('utf-8'), headers)
        except:
            raise Exception, _('Server Unavailable')
        
        if "returnMessage" in content:
            root = etree.XML(content)
            message = root.find(".//runReportReturn").text.replace('\n', '')
            raise Exception, message
        
        if resp['status'] != '200' or "CDATA[Error" in content:
            raise Exception, content
        
        msg = Message.load(cStringIO.StringIO(content)).records
        reponse = None
        for x in msg:
            if 'uuid:' not in x.id:
                if not reponse:
                    reponse = x.data
                else:
                    reponse = reponse+x.data

        if not reponse:
            raise Exception, _('Report Empty \r\n %s \r\n %s ' % (resp, content))
        format = data['form']['params'][0].lower()
        
        if 'attachment' in data['form']:
            attachment_obj = pool.get('ir.attachment')
            attachment_obj.create(cr, uid, {'name': data['form']['attachment']['name'],
                                            'datas': base64.encodestring(reponse),
                                            'datas_fname': '%s.pdf' % data['form']['attachment']['name'],
                                            'res_model': data['form']['attachment']['object'],
                                            'res_id': data['form']['attachment']['id']}, context=context)

        return (reponse, format)


report_custom('report.print.jasper.pdf')
